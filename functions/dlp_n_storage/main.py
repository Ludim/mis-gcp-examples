import logging

import google.cloud.dlp
import google.cloud.storage as storage

project_id = 'tu_project_id'

def dlp_file_detection(data, context):

    storage_client = storage.Client()
    dlp_client = google.cloud.dlp.DlpServiceClient()

    # info_types a buscar en la cadena que es inspeccionada. Existe una lista de elementos predefinidos por la API
    info_types = [{'name': 'FIRST_NAME'}, {'name': 'LAST_NAME'}, {'name': 'EMAIL_ADDRESS'},
                  {'name': 'MEXICO_CURP_NUMBER'}]

    # La probabilidad inferior aceptable para indicar que se ha encontrado una coincidencia.
    min_likelihood = 'POSSIBLE'

    # El valor superior de hallazgos que son reportados (0 = server maximum)
    max_findings = 0

    # True cuando se quiera incluir la cadena identificada en el resultado
    include_quote = False

    inspect_config = {
        'info_types': info_types,
        'custom_info_types': [],
        'min_likelihood': min_likelihood,
        'include_quote': include_quote,
        'limits': {'max_findings_per_request': max_findings},
    }

    parent = dlp_client.project_path(project_id)

    # mediante data se pueden extraer algunos elementos como el nombre del archivo a analizar y su bucket
    bucket = storage_client.get_bucket(data['bucket'])
    blob = bucket.get_blob(data['name'])
    # y tal archivo se declara como el item indispensable para llamar a la DLP API
    item = {'byte_item': {'type': None, 'data': blob.download_as_string()}}

    # llamado a la API.
    response = dlp_client.inspect_content(parent, inspect_config, item)

# Imprimir la respuesta
    if response.result.findings:
        for finding in response.result.findings:
            try:
                logging.info('Hallazgo: {}'.format(finding.quote))
            except AttributeError:
                pass
            logging.info('Info type: {}'.format(finding.info_type.name))
            likelihood = (google.cloud.dlp_v2.types.Finding.DESCRIPTOR
                          .fields_by_name['likelihood']
                          .enum_type.values_by_number[finding.likelihood]
                          .name)
            logging.info('Likelihood: {}'.format(likelihood))
    else:
        logging.info('Sin hallazgos')

if __name__ == '__main__':
    dlp_file_detection()
