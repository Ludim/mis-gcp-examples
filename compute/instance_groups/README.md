# Compute Engine - Instance Groups

Un ejemplo de cómo crear un grupo de máquinas virtuales listas para escalar de acuaerdo a algún critero que hayamos 
definido y se conecta con un http global load balancer para atender las solicitudes.

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true).

##### 3. 

* Crea tu 'instance template' que será la referencia para las VM de tu 'instance group'

```bash
gcloud compute instance-templates create el-nombre-de-tu-instance-template \
--machine-type=n1-standard-1 \
--network=projects/myxerticalabsproject/global/networks/default --network-tier=PREMIUM \
--image-family=debian-9 --image-project=debian-cloud \
--boot-disk-size=100GB --boot-disk-type=pd-standard --boot-disk-device-name=el-nombre-de-tu-disco \
--metadat=startup-script-url=gs://TU_BUCKET/tu_startupscript.sh
``` 

* donde tu_startupscript.sh puede ser algo parecido a:

```bash
#! /bin/bash

sudo apt update
sudo apt install -y apache2 zip
cd /var/www/html
sudo rm index.html -f
sudo wget https://mi_ruta_publica/mdl-template-android-dot-com.zip
sudo unzip mdl-template-android-dot-com.zip
```

Y de esta manera tener un contenido html servido desde las VM creadas basadas en esta plantilla

##### 4. 

* Ahora que tienes un 'instance template' puedes crear tu 'instance groups' ejecutando:

```bash
gcloud compute instance-groups managed create el-nombre-de-tu-instance-group \
--base-instance-name=el-nombre-de-tu-instance-group \
--template=el-nombre-de-tu-instance-template \
--size=2 \
--region "us-central1"
```

* y luego configura los criterios para que pueda escalar usando como referencia la carga del load balancer

```bash
gcloud compute instance-groups managed \
set-autoscaling "el-nombre-de-tu-instance-group" --region "us-central1" \
--cool-down-period "60" --max-num-replicas "6" --min-num-replicas "2" \
--target-load-balancing-utilization "0.8"
```

##### 5.

Revisa nuestro [video]() para conocer cómo puedes conectar el HTTP Global Load Balancer a tu instance group

#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)