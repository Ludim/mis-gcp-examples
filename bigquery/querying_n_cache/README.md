# Big Query, usando cache

Ejemplo de un consulta en Big Query para extraer información de un public dataset y luego configurarla para que use el 
cache del servicio 

## Prueba tu script

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

* Crea tu entorno virtual

```
virtualenv -p python3 my_bq_venv
source my_bq_venv/bin/activate
```

##### 4.

* Resuelve tus dependencias

```
pip install -r requirements.txt
``` 

##### 5. 

* [Crea](https://cloud.google.com/dlp/docs/libraries#setting_up_authentication) una cuenta de servicio con los 
privilegios mínimos necesarios y obtén el correspondiente archivo .json

* Asigna a la variable `GOOGLE_APPLICATION_CREDENTIALS` la ruta de tu archivo .json
  
Ejemplo para Linux y MacOS
```bash
export GOOGLE_APPLICATION_CREDENTIALS="/La/Ruta/De/Tu/Archivo.json"
```
Ejemplo para Windows Power Shell
```
$env:GOOGLE_APPLICATION_CREDENTIALS="C:\La\Ruta\De\Tu\Archivo.json"
```

##### 6.

* Ejecuta tu script

````python
python query_BQ.py
````