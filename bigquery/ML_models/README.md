# BigQuery ML
Un ejemplo sencillo de cómo, mediante consultas SQL, se puede crear un modelo de Machine Learning, en este caso, usando 
regresión lineal para predecir ciertos datos.

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true).

##### 3. 

* Crea tu [dataset](https://cloud.google.com/bigquery/docs/datasets) y una [tabla](https://cloud.google.com/bigquery/docs/tables#create-table) 
con los datos que desees utilizar para construir tu modelo. Para este caso se uso un conjunto de datos abiertos por el 
Gobierno de la Ciudad de Mexico acerca de la calidad del aire en la zona metropolitana. Puedes consultarlos [aqui](http://www.datosabiertos.cdmx.gob.mx:5000/dataset/indice-de-la-calidad-del-aire)

##### 4.

* Crea tu modelo

```sql
# El modelo se crea en el mismo dataset en el cual estamos trabajando
CREATE OR REPLACE MODEL `tu_dataset.modelo_ambiental` 
# se indica el tipo de modelo a usar
OPTIONS (model_type='linear_reg', input_label_cols=['Sureste_mon__xido_de_carbono']) AS
# se proveen los datos que emplea el modelo 
SELECT
  Sureste_Ozono,
  Sureste_di__xido_de_azufre,
  Sureste_di__xido_de_nitr__geno,
  Sureste_PM10,
  Sureste_mon__xido_de_carbono
FROM
  `tu_dataset.cdmx_ambiental_13_17`
WHERE
  Sureste_mon__xido_de_carbono IS NOT NULL
```

* Evaluación del modelo

```sql
SELECT * FROM
ML.EVALUATE(MODEL `tu_dataset.modelo_ambiental`,
# evaluamos el modelo contra los datos
  (
  SELECT
  Sureste_Ozono,
  Sureste_di__xido_de_azufre,
  Sureste_di__xido_de_nitr__geno,
  Sureste_PM10,
  Sureste_mon__xido_de_carbono
FROM
  `tu_dataset.cdmx_ambiental_13_17`
WHERE
  Sureste_mon__xido_de_carbono IS NOT NULL
  )
)
LIMIT 100
```

* Predicción con el modelo

```sql
SELECT * FROM
ML.PREDICT(MODEL `tu_dataset.modelo_ambiental`,
  (
  SELECT
  Sureste_Ozono,
  Sureste_di__xido_de_azufre,
  Sureste_di__xido_de_nitr__geno,
  Sureste_PM10,
  Sureste_mon__xido_de_carbono
FROM
  `tu_dataset.cdmx_ambiental_13_17`
WHERE
  Sureste_mon__xido_de_carbono IS NOT NULL
  )
)
LIMIT 100
```

Ahora, a partir de los datos que tienes, cuentas con un mecanismo que te permite establecer predicciones . Recuerda que 
la calidad de tus datos y la eleccion de valores involucrados incidira significativamente en los resultados.

#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)