# Google Cloud Storage

Un ejemplo de las posibilidades de Google Cloud Platform. Se ilustran tales posibilidades a través del Google Cloud SDK 
mediante el comando gsutil y la biblioteca para python.

## Haz una gestión versátil de tus archivos

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

###### 3.1

Crea un bucket

```bash
gsutil mb gs://TU_BUCKET
```

###### 3.2

Almacena un archivo

```bash
gsutil cp /LA/RUTA/DE/TU/ARCHIVO gs://TU_BUCKET
```

###### 3.3

Habilitar versionamiento

```bash
gsutil versioning set on gs://TU_BUCKET
```

Conoce el estatus del versionamiento

````bash
gsutil versioning get gs://TU_BUCKET
````

Lista toda las versiones existentes y sus respectivos identificadores únicos

````bash
gsutil ls -a gs://TU_BUCKET/TU_FILE
````

Recupera una versión específica y produce un nuevo archivo

````bash
gsutil cp gs://TU_BUCKET/TU_FILE#GENERATION_NUMBER_X gs://BUCKET_DESTINO/NOMBRE_DE_TU_FILE
````

Borra una versión específica

````bash
gsutil rm gs://TU_BUCKET/TU_FILE#GENERATION_NUMBER_X
````

###### 3.4

Despliega un .json para definir las reglas automáticas que manejan los ciclos de 
vida de tus objetos almacenados

```bash
gsutil lifecycle set TU_LIFECYCLE_FILE gs://TU_BUCKET/
```

##### 4.

* Crea tu entorno virtual

```
virtualenv -p python3 my_storage_venv
source my_storage_venv/bin/activate
```

##### 5.

* Resuelve tus dependencias

```
pip install -r requirements.txt
``` 

##### 6. 

* [Crea](https://console.cloud.google.com/iam-admin/serviceaccounts) una cuenta de servicio con los 
privilegios mínimos necesarios y obtén el correspondiente archivo .json

* Asigna a la variable `GOOGLE_APPLICATION_CREDENTIALS` la ruta de tu archivo .json
  
Ejemplo para Linux y MacOS
```bash
export GOOGLE_APPLICATION_CREDENTIALS="/La/Ruta/De/Tu/Archivo.json"
```
Ejemplo para Windows Power Shell
```
$env:GOOGLE_APPLICATION_CREDENTIALS="C:\La\Ruta\De\Tu\Archivo.json
```

##### 7. 

Ejecuta tu Python script para subir y descargar un archivo usando tus propias 
key encryption

```bash
python main.py
```

#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)
