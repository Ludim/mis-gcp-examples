# DLP Análisis Personalizado

Ejemplo de Data Loss Prevention API, con Python Client Library, para buscar datos específicos mediante constantes 
predefinidas por la API. También se incorporan expresiones regulares y diccionarios personalziados para detectar datos 
puntuales no definidos por las constantes de la API.

Con las constantes buscamos detectar: 
* nombre(s)
* apellido(s)
* CURP - cédula de identidad mexicana

Con las personalizaciones buscamos detectar:
* RFC - registro federal de contribuyentes mexicanos
* Diccionarios - Un listado específico de palabras (rfc, curp y registrado)

## Prueba tu script

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

* Crea tu entorno virtual

```
virtualenv -p python3 my_dlp_venv
source my_dlp_venv/bin/activate
```

Nota: Si prefieres usar python2, asegúrate de descomentar la línea 15 de `dlp_custom_detection.py`

##### 4.

* Resuelve tus dependencias

```
pip install -r requirements.txt
``` 

##### 5. 

* [Crea](https://cloud.google.com/dlp/docs/libraries#setting_up_authentication) una cuenta de servicio con los 
privilegios mínimos necesarios y obtén el correspondiente archivo .json

* Asigna a la variable `GOOGLE_APPLICATION_CREDENTIALS` la ruta de tu archivo .json
  
Ejemplo para Linux y MacOS
```bash
export GOOGLE_APPLICATION_CREDENTIALS="/La/Ruta/De/Tu/Archivo.json"
```
Ejemplo para Windows Power Shell
```
$env:GOOGLE_APPLICATION_CREDENTIALS="C:\La\Ruta\De\Tu\Archivo.json
```

##### 6.

* Ejecuta tu script

````python
python dlp_custom_detection.py
````

Nota: Para que puedas completar correctamente esta parte del ejercicio no olvides asegurar que está habilitada 
[DLP API](https://console.cloud.google.com/apis/api/vision.googleapis.com/overview)
en tu proyecto de GCP. 


#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)
